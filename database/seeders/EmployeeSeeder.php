<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\employee;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        employee::truncate();
        employee::create([
            'name' => 'Pak Budi',
            'company_id' => 1,
        ]);


    }
}
