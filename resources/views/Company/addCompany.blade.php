<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <title>AdminLTE 3 | Starter</title>
  @include('Templates.head')
  <script src="{{ asset('Js/watch.js') }}"></script>
</head>
<body class="hold-transition sidebar-mini" onload="realtimeClock()">
<div class="wrapper">

  <!-- Navbar -->
    @include('Templates.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Templates.left-sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container">
        <div class="row">
            <div class="col-12 mt-5">
                <h3>Form Input</h3>
                <form action="{{ route('save-company') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-floating mb-3">
                        <label for="nama">Company</label>
                        <input type="text" class="form-control" placeholder="input nama company" id="nama" name="nama">
                      </div>
                      <div class="form-floating">
                          <label for="alamat">Alamat Company</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" placeholder="input alamat company">
                      </div>
                      <div class="form-floating mt-3">
                          <button type="submit" class="btn btn-success">Simpan Data</button>
                      </div>
                  </div>
                </form>
               

            </div>
        </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <div class="mb-5"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@include('Templates.script')

</body>
</html>
