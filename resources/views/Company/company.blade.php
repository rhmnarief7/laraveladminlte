<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <title>AdminLTE 3 | Starter</title>
  @include('Templates.head')
  <script src="{{ asset('Js/watch.js') }}"></script>
</head>
<body class="hold-transition sidebar-mini" onload="realtimeClock()">
<div class="wrapper">

  <!-- Navbar -->
    @include('Templates.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Templates.left-sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container">
          <div class="d-flex align-items-center" style="justify-content: space-between;">
              <h2>Database Company</h2>
                <div class="button">
                    <a href="{{ route('add-company') }}" class="btn btn-success">Tambah Data<i class="fas fa-plus-square"></i></a>
                </div>
          </div>
        <div class="row">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Company</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($dataCompany as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->alamat }}</td>
                        <td>
                            <a href="{{ url('edit-company',$item->id) }}"><i class="fas fa-edit"></i></a> | <a href="{{ url('delete-company',$item->id) }}"><i class="fas fa-trash-alt" style="color:red;"></i></a>
                        </td>
                    </tr>
                    @endforeach
                 
                </tbody>
              </table>
              
        </div>
     

      </div>
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <div class="mb-5"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@include('Templates.script')
@include('sweetalert::alert')

</body>
</html>
