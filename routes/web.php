<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PresentController;
use App\Http\Controllers\calculateController;
use App\Http\Controllers\employeeController;
use App\Http\Controllers\CompanyController;
use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
route::post('/',[calculateController::class, 'calculate'])->name('calculate');

route::post('/calculate',[calculateController::class, 'checkNumber'])->name('checkNumber');
route::post('/notation',[calculateController::class, 'checkNotation'])->name('checkNotation');




route::get('/login',[LoginController::class, 'loginPage'])->name('login');
route::post('/postLogin',[LoginController::class, 'postLogin'])->name('postLogin');
route::get('/logout',[LoginController::class, 'logout'])->name('logout');

route::get('/register',[LoginController::class, 'register'])->name('register');
route::post('/sendRegister',[LoginController::class, 'sendRegister'])->name('sendRegister');


Route::group(['middleware' =>['auth','checkLevel:admin,karyawan']], function () {
    route::get('/home',[HomeController::class, 'index'])->name('home');
    route::get('/employee',[employeeController::class, 'index'])->name('employee');
    route::get('/add-employee',[employeeController::class, 'create'])->name('addEmployee');
    route::post('/save-employee',[employeeController::class, 'store'])->name('saveEmployee');
    route::get('/edit-employee/{id}',[employeeController::class, 'edit'])->name('edit-employee');
    route::post('/update-employee/{id}',[employeeController::class, 'update'])->name('update-employee');
    route::get('/delete-employee/{id}',[employeeController::class, 'destroy'])->name('delete-employee');

    route::get('/company',[CompanyController::class, 'index'])->name('company');
    route::get('/add-company',[CompanyController::class, 'create'])->name('add-company');
    route::post('/save-company',[CompanyController::class, 'store'])->name('save-company');
    route::get('/edit-company/{id}',[CompanyController::class, 'edit'])->name('edit-company');
    route::post('/update-company/{id}',[CompanyController::class, 'update'])->name('update-company');
    route::get('/delete-company/{id}',[CompanyController::class, 'destroy'])->name('delete-company');
    
});
Route::group(['middleware' =>['auth','checkLevel:karyawan']], function () {
    route::post('/simpan-masuk',[PresentController::class, 'store'])->name('simpan-masuk');
    route::get('/presensi-masuk',[PresentController::class, 'index'])->name('presensi-masuk');

});