<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    
    use HasFactory;
    public $timestamps = false;
    protected $table = 'employees';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'name',
        'atasan_id',
        'company_id',
    ];
}
