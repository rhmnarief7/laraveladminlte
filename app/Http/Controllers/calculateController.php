<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LengthException;

class calculateController extends Controller
{
    //
    public function index(){
        return view ('welcome');
    }
    public function calculate(Request $request){
        $number1=$request->number1;
        $number2=$request->number2;
        $operator=$request->operator;

        if($operator == '+'){
            $output = $number1 +  $number2;
            return view('welcome', compact('output'));
        }elseif($operator == '-'){
            $output = $number1 -  $number2;
            return view('welcome', compact('output'));
        }elseif($operator == '/'){
            $output = $number1 /  $number2;
            return view('welcome', compact('output'));
        }elseif($operator == '*'){
            $output = $number1 *  $number2;
            return view('welcome', compact('output'));
        }
    }
    public function checkNumber(Request $request){
        $checkNumber1=$request->checkNumber1;
        $checkNumber2=$request->checkNumber2;

        if($checkNumber1 % 2 == 0 && $checkNumber2 % 2 == 0 ){
            $validate = "Bilangan $checkNumber1 dan $checkNumber2 merupakan bilangan genap";
            return view('welcome', compact('validate'));
        }elseif($checkNumber1 % 2 != 0 && $checkNumber2 % 2 == 0 ){
            $validate = "Bilangan $checkNumber1  meruapakan bilangan ganjil dan $checkNumber2 merupakan bilangan genap";
            return view('welcome', compact('validate'));
        }elseif($checkNumber1 % 2 == 0 && $checkNumber2 % 2 != 0 ){
            $validate = "Bilangan $checkNumber1  meruapakan bilangan genap dan $checkNumber2 merupakan bilangan ganjil";
            return view('welcome', compact('validate'));
        }else{
            $validate = "Bilangan $checkNumber1 dan $checkNumber2 merupakan bilangan ganjil";
            return view('welcome', compact('validate'));
        }
    }
    public function checkNotation(Request $request){
        $checkText=$request->checkText;
        $countVocal = 0;
        $a = 0;
        $i = 0;
        $u = 0;
        $e = 0;
        $o = 0;
        for ($j=0; $j < strlen($checkText) ; $j++) { 
            if($checkText[$j] == 'a'){
                $a = $a +1;
             }elseif($checkText[$j] == 'i'){
                $i = $i +1;
            }elseif($checkText[$j] == 'u'){
                $u = $u +1;
            }elseif ($checkText[$j] == 'e'){
                $e = $e +1;
            }elseif ($checkText[$j] == 'o'){
                $o = $o +1;
            }
            }
        $countVocal = $a + $i + $u +$e +$o;
        return view('welcome',compact('countVocal'));
    }
}
