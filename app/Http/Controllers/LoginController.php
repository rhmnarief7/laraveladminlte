<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
    //
    public function loginPage(){
        return view('Login.login');
    }
    public function postLogin(Request $request){
        if(Auth::attempt($request->only('email','password'))){
            return redirect('/home');
        }
        return redirect('/');
    }
    public function logout(Request $request){
        Auth::logout();
        return redirect('/');
    }
    public function register(Request $request){
        return view('Login.register');
    }
    public function sendRegister(Request $request){
        User::create([
            'name' => $request -> name,
            'level' => 'karyawan',
            'email' => $request -> email,
            'password' => bcrypt($request -> password),
            'remember_token' => Str::random(60),
        ]);
        return view('welcome');
    }
}
