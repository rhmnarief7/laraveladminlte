<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\employee;

class employeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataEmployee = employee::paginate(2);
        return view('Employee.employee', compact('dataEmployee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Employee.AddEmployee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        employee::create([
            'name' =>$request->name,
            'atasan_id' =>$request->atasan_id,
            'company_id' =>$request->company_id,
        ]);
        return redirect('employee')->with('toast_success', 'Data Created Successfully!');
    }
  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function goto($id){
        $employee = employee::findorfail($id);
        return view('Employee.UpdateEmployee', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee = employee::findorfail($id);
        return view('Employee.UpdateEmployee', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $employee = employee::findorfail($id);
        $employee->update($request->all());
        return redirect('employee')->with('toast_success', 'Data Update Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $employee = employee::findorfail($id);
        $employee->delete();
        return back()->with('toast_success', 'Data Deleted Successfully!');

    }
}
